/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/
const resultBlock = document.createElement("div");
resultBlock.id = "CommentsFeed";
document.body.appendChild(resultBlock);

function Comment(name = "unnamed", text = "", avatarUrl = 'images/cat2.jpg') {
  this.name = name;
  this.text = text;
  this.avatarUrl =avatarUrl;
  this.likes=0;

  this.incLikes = function() {
    this.likes++;
    ShowComments();
  }

    
  }

let comment1 = new Comment("kak vam kot", "xa-xa ,ne xochet" );
let comment2 = new Comment("ymniy kot", "krasavela" );
let comment3 = new Comment("doigralsya kot", "mnogo igral,vot i doigralssya" );



let CommentsArray = [comment1, comment2, comment3]


function ShowComments(commentsArray) {
   this.commentsArray = commentsArray;
  this.show = function() {
      this.commentsArray.forEach(comment => {
     
    
         const newName = document.createElement("h3");
         newName.innerText = comment.name;
         resultBlock.appendChild(newName);


         const newImg = document.createElement("img");
         newImg.src = comment.avatarUrl;
         newImg.style.width = "75px";
         resultBlock.appendChild(newImg);


         const newText = document.createElement("p");
         newText.innerText = comment.text;
         resultBlock.appendChild(newText);


         const btnLike = document.createElement("button");
         btnLike.innerText = "likes: " + comment.likes;

         btnLike.addEventListener("click", evt => {
          comment.incLikes();
          btnLike.innerText = "likes: " + comment.likes;
         })
         resultBlock.appendChild(btnLike);

         

      })
     
    }
}


let displayComments = new ShowComments(CommentsArray);

displayComments.show()

 comment1.incLikes();
// console.log(comment1.likes);
 comment1.incLikes();
 //console.log(comment1.likes);



/*
 let a1  = {};

   Object.setPrototypeOf( a1, Comment.prototype );
     var myComment1 = new Comment("ne ygomovniy kot","xa-xa ,ne xochet");
console.log(a1)*/